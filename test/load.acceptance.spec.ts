import load from '../src/load';


const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;


describe('loadTestSuite()', function () {
    context('given a workspace', () => {
        it('should load test suite', () => {
            return expect(load('test'))
                .to.eventually.deep.equal(
                    {
                        type: 'suite',
                        id: 'root',
                        label: 'behave',
                        children: [
                            {
                                type: 'suite' as const,
                                id: 'fake feature',
                                label: 'F: fake feature',
                                debuggable: false,
                                file: 'test/fake-workspace/features/fake.feature',
                                children: [
                                    {
                                        type: 'test',
                                        id: 'fake feature:successful scenario',
                                        label: 'S: successful scenario',
                                        file: 'test/fake-workspace/features/fake.feature',
                                        line: 2,
                                    },
                                    {
                                        type: 'test',
                                        id: 'fake feature:failing scenario',
                                        label: 'S: failing scenario',
                                        file: 'test/fake-workspace/features/fake.feature',
                                        line: 7,
                                    },
                                    {
                                        type: 'test',
                                        id: 'fake feature:successful scenario outline',
                                        label: 'SO: successful scenario outline',
                                        file: 'test/fake-workspace/features/fake.feature',
                                        line: 12,
                                    },
                                ]
                            },
                            {
                                type: 'suite' as const,
                                id: 'super feature',
                                label: 'F: super feature',
                                debuggable: false,
                                file: 'test/fake-workspace/features/sub-folder/super.feature',
                                children: [
                                    {
                                        type: 'test',
                                        id: 'super feature:successful super scenario',
                                        label: 'S: successful super scenario',
                                        file: 'test/fake-workspace/features/sub-folder/super.feature',
                                        line: 2,
                                    },
                                    {
                                        type: 'test',
                                        id: 'super feature:failing super scenario',
                                        label: 'S: failing super scenario',
                                        file: 'test/fake-workspace/features/sub-folder/super.feature',
                                        line: 7,
                                    },
                                    {
                                        type: 'test',
                                        id: 'super feature:successful super scenario outline',
                                        label: 'SO: successful super scenario outline',
                                        file: 'test/fake-workspace/features/sub-folder/super.feature',
                                        line: 12,
                                    },
                                ]
                            }
                        ]
                    }
                )
            }
        );
    })
});
